package apps.ecommtest.components.tekno.toyComponent;
import com.adobe.cq.sightly.WCMUsePojo;
import com.tekno.ecomm.core.business.ToyManagementService;

public class ToyComponentHelper extends WCMUsePojo{
	
	String rootpath;
	
	String staticProp;
	//called when the UI Component is rendered
	
	public void activate() {
		ToyManagementService toyManagementservice = getSlingScriptHelper().getService(ToyManagementService.class);
		
		rootpath = toyManagementservice.getRootPath();
		staticProp = toyManagementservice.getStaticProp();
	}
	
	public String getRootPath()
	{
		return rootpath;
	}
	
	public String getStaticProp()
	{
		return staticProp;
	}
}