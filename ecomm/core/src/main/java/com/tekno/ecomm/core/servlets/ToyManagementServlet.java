package com.tekno.ecomm.core.servlets;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;

import org.apache.sling.api.SlingHttpServletResponse;

import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.osgi.service.component.annotations.Component;

import org.osgi.service.component.annotations.Reference;

import org.slf4j.Logger;
import java.util.List;
import java.util.Set;

import org.slf4j.LoggerFactory;

import com.day.cq.commons.TidyJSONWriter;
import com.day.cq.commons.TidyJsonItemWriter;
import com.tekno.ecomm.core.business.ToyManagementService;

import com.tekno.ecomm.core.models.ToyModel;

@Component(service = Servlet.class, property = { "sling.servlet.methods=get", "sling.servlet.methods=post",

		"sling.servlet.paths=/tekno/toys" })

// older version

// @SlingServlet

public class ToyManagementServlet extends SlingAllMethodsServlet {

	@Reference

	ToyManagementService toyService;

	public void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
		  
	
		try {
			List<ToyModel>  toys = toyService.getAllToys();
			//response.getWriter().println(toys.get(0).toString());
			TidyJSONWriter writer = new TidyJSONWriter(response.getWriter());
			writer.object();
			writer.setTidy("true".equals(request.getParameter("tidy")));
			writer.key("page").value(1);
			writer.key("total").value(toys.size());
			writer.key("rows").array();
			
			for(ToyModel toyModel : toys) {
				writer.object();
				writer.key("name").value(toyModel.getToyName());
				writer.key("cell").array();
				writer.value(toyModel.getToyName());
				writer.value(toyModel.getToyDescription());
				writer.value(toyModel.getToyPrice());
				writer.endArray();
				writer.endObject();
			}
			
			writer.endArray();
			writer.endObject();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		}
	
	
/*
	public void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {

		String toyName = request.getParameter("toy-name");

		ToyModel toyModel = new ToyModel(request.getParameter("toy-name"), request.getParameter("toy-desc"),
				request.getParameter("toy-price"));

	response.getWriter().print(toyModel.toString());
	
	
	toyService.addToy(toyModel);

	response.getWriter().print("Node Added");
	
	
	
	n 	toyService.updateToy(toyModel);

		response.getWriter().print("Node updated");

	}*/
	
	@Override
	protected void doDelete(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		String toyName = request.getParameter("toy-name");

		ToyModel toyModel = new ToyModel(request.getParameter("toy-name"), request.getParameter("toy-desc"),
				request.getParameter("toy-price"));

		toyService.deleteToy(toyModel);

		response.getWriter().print("Node Deleted");
	}
	
	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		String toyName = request.getParameter("toy-name");

		ToyModel toyModel = new ToyModel(request.getParameter("toy-name"), request.getParameter("toy-desc"),
				request.getParameter("toy-price"));

		toyService.updateToy(toyModel);

		response.getWriter().print("Node updated");
	}

}
