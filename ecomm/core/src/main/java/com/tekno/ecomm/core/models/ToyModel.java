package com.tekno.ecomm.core.models;

//import javax.annotation.Resource;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
@Model(adaptables= {Resource.class})
public class ToyModel {
	
	public ToyModel() {
		
	}

	public ToyModel(String toyName, String toyDescription, String toyPrice) {

		this.toyName = toyName;
		this.toyDescription = toyDescription;
		this.toyPrice = toyPrice;

	}
	@Inject
	private String toyName;
	@Inject @Named("toyDesc")
	private String toyDescription;
	@Inject
	private String toyPrice;
	

	
	public String getToyName() {
		return toyName;
	}
	
	public String getToyDescription() {
		return toyDescription;
	}

	public String getToyPrice() {
		return toyPrice;
	}
	
	public String toString() {
		
	  return   this.toyName +"::"+this.toyDescription+"::"+this.toyPrice;
	  
	}

}
