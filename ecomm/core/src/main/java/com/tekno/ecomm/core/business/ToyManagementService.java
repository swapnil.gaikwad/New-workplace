package com.tekno.ecomm.core.business;

import java.util.List;

import com.tekno.ecomm.core.models.ToyModel;

public interface ToyManagementService {

	public String getRootPath();

	public void addToy(ToyModel toyModel);

	public ToyModel getToyByName(String toyName);

	public String getStaticProp();

	public List<ToyModel> getAllToys();

	public String updateToy(ToyModel toymodel);

	public String deleteToy(ToyModel toymodel);

}
